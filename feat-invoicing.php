	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="light white">Collect Payments With Ease</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl"><a href="mailto:sales@merchantocracy.com" class="white">Email Us To Set Up Trial Offer</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials" class="section">
		<div class="container">
			<div class="row title text-center margin-around">
				<h2>Manage Bills</h2>
			</div>
			<!-- Start 1 -->
			<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Are you tired of manually keeping a ledger of accounts or bills that need to be paid, only to loose your notes somewhere and have to do the work all over again?  Merchantocracy Invoicing allows you  to connect to your account recievables so that you can manage your entire business in one place.
								   </h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-invoicing/01_manage_bills.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 1 -->	
			<!--<div class="row title text-center margin-around">
				<h2>Hosted or 3rd Party Hosted Solutions</h2>
			</div>-->
			<!-- Start 2 -->
			<!--<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Clock employee time and billable hours with ease, our platform provides you to manage weekly timesheets so that you track project/task cost day to day, to manage cost expenditures and ensure more accurate reporting to your clients and internally to your team.
								   </h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-invoicing/02_track_time.png" alt="" class="img-responsive center-block"></div>
     </div>-->
     <!-- End 2 -->
	 <div class="row title text-center margin-around">
				<h2>Track Income & Expenses</h2>
			</div>
			<!-- Start 3 -->
			<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-invoicing/03_track_income_expenses.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   As a business owner, you need to know where your money is flowing. With Merchantocracy, you can easy organize your business finances in one place.  We can help you connect payment options to current outstanding bills, automatically sort transactions and create custom rules for automation.
								   </h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 3 -->	
			<div class="row title text-center margin-around">
				<h2>Maximize Tax Deductions</h2>
			</div>
			<!-- Start 4 -->
			<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Don't leave your tax management until tax time, save yourself, your life and your company by maximizing tax deductions year round for peace of mind.  Get the return you've earned.
								   </h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-invoicing/04_max_tax_deductions.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 4 -->
	 <div class="row title text-center margin-around">
				<h2>Invoice & Accept Payments</h2>
			</div>
			<!-- Start 5 -->
			<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-invoicing/05_accept_payments.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								    With Merchantocracy you can create and send invoices to get paid faster.  Our invoicing platform helps you get paid fast by requesting payment in the form of credit card (Visa, MasterCard or American Express).
									</h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 5 -->	
			<div class="row title text-center margin-around">
				<h2>Run Reports</h2>
			</div>
			<!-- Start 6 -->
			<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Knowing the financial status of your business in real time is critical to making informed business decisions that positively effect your business outcomes.  With Merchantocracy our reporting system allows you to know exactly where your business stands so that you can see the big picture.  Get a grip on how much your making, what's your cashflow and who owes you so that you can enjoy a clear understanding of where your going.
								   </h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-invoicing/06_run_reports.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 6 -->
	<div class="row title text-center margin-around">
				<h2>Send Estimates</h2>
			</div>
			<!-- Start 7 -->
			<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-invoicing/07_send_estimates.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								    Sometimes you need to send an estimate/quote to your customer so that they can more accurately budget and project resource utilization for your services.  Share estimates to land the billable opporutnity and seal the deal on the spot. 
									</h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 7 -->
	 <div class="row title text-center margin-around">
				<h2>3rd Party Integrations</h2>
			</div>
			<!-- Start 8 -->
			<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   We can connect your Merchantocracy account to any 3rd party provider via our API.  All you need to know is that our developers are ready to help you and make sure that you get the service that matches your business needs exactly, our dev team will help you achieve this objective.
								   </h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-invoicing/08_3rd_party_int.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 8 -->
	 </div>
	</section>
	<section class="section section-bot-padded section-transparent">
	</section>
	<section class="section section-slick-padded call-to-action">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="drk regular">Limited Time Offer</h3>
							<h4 class="drk regular">Sign up today and receive a 33% discount on your Invoicing solution set up costs when you enrol before July 2018</h4>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="text-center">
							<h3 class="btn btn-xmain-xl"><a href="mailto:sales@merchantocracy.com" class="white" data-toggle="modal" data-target="#modal1">Ask us about our Free Merchant Program</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>