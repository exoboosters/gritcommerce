<?php
$smtp = new SMTP ( 'smtp.zoho.com', 465, 'SSL', 'admin@merchantocracy.com', 'fr33radin$' );

$smtp->set('From', '"Merchantocracy" <admin@merchantocracy.com>');
$smtp->set('To', '<sales@merchantocracy.com>');
$smtp->set('Subject', 'Somebody is interested.. Time for a follow-up');  
$smtp->set('Errors-to', '<admin@merchantocracy.com>'); 

$message = "Hello!\n Here is a potential lead. Time to follow-up\n";
$message .= "\nDetails are :\n";
$message .= "\nFirst Name: ".$f3->get('POST.fname');
$message .= "\nLast Name: ".$f3->get('POST.lname');
$message .= "\nCompany: ".$f3->get('POST.cname');
$message .= "\nWebsite: ".$f3->get('POST.website');
$message .= "\nEmail: ".$f3->get('POST.email');
$message .= "\nPhone: ".$f3->get('POST.phone');
$message .= "\nBest Time to call: ".$f3->get('POST.sel-best-time');
$message .= "\nIndustry: ".$f3->get('POST.industry'); 
$message .= "\n\nTime data posted: ".date('D F j, Y, g:i a');
$message .= "\n\nThanks,\nMtcyBot";

$sent = $smtp->send($message, TRUE);

$mylog = $smtp->log;

$sentText = 'Not sent';

if ($sent)
{
    $sentText = 'Was sent';
}

echo "<!--email result: " . $sentText . ",mylog: " . $mylog . "-->";

?>
	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3 class="light white typed">When we finalize a time to review our demo, coffee/tea is on us!</h3>
							<span class="typed-cursor">|</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl">Thank you</h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section section-slick-padded blue-bg">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="white light">Thank you</h3>
							<h5 class="white light">We have received your details as below. We will contact you soon.</h5>
							<h5>&nbsp;</h5>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<h6 class="white">First Name</h6>
					<h5 class="white light"><?php echo $f3->get('POST.fname');?></h5>
				</div>
				<div class="col-md-6">
					<h6 class="white">Last Name</h6>
					<h5 class="white light"><?php echo $f3->get('POST.lname');?></h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<h6 class="white">Company Name</h6>
					<h5 class="white light"><?php echo $f3->get('POST.cname');?></h5>
				</div>
				<div class="col-md-6">
					<h6 class="white">Website</h6>
					<h5 class="white light"><?php echo $f3->get('POST.website');?></h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<h6 class="white">Email Address</h6>
					<h5 class="white light"><?php echo $f3->get('POST.email');?></h5>
				</div>
				<div class="col-md-6">
					<div class="dropdownf">
						<h6 class="white">Best time to call</h6>
						<h5 class="white light"><?php echo $f3->get('POST.sel-best-time');?></h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<h6 class="white">Phone Number</h6>
					<h5 class="white light"><?php echo $f3->get('POST.phone');?></h5>
				</div>	
				<div class="col-md-6">
					<h6 class="white">Industry</h6>
					<h5 class="white light"><?php echo $f3->get('POST.industry');?></h5>
				</div>
			</div>
		</div>
	</section>
	<section>
			<div class="cut cut-bottom-lb"></div>
	</section>
	
	<!-- Tweets -->
	<?php include 'tweets.php'; ?>
	<!-- End Tweets -->