	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="light white">Upgrade Your Payment Experience With Lightning Fast Terminals</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl"><a href="mailto:sales@merchantocracy.com" class="white">Email Us To Set Up Trial Offer</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials" class="section">
		<div class="container">
			<!-- Start 1 -->
			<div class="row title text-center margin-around">
				<h2>Cloud Software Enabled</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-pos/01_cloud_software_enabled.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   Operating POS terminals can cost you time and money. Especially when payment networks upgrade servers such that new software upgrades are required.  All of our terminals are cloud enabled so software is uploaded directly to the terminal with a few easy steps.  This ultimately saves you a lot of time and effort so you can spend more energy on your business.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 1 -->	
			<div class="row title text-center margin-around">
				<h2>Customizable Settings</h2>
			</div>
			<!-- Start 2 -->
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							Not all businesses are the same, with Mechantocracy POS, you can add tip settings, tax rates, customer experience ratings to your POS payment process workflow.  Nothing is beyond the possibility of what our terminals can do.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-pos/02_customizable_settings.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 2 -->
			<!-- Start 3 -->
			<div class="row title text-center margin-around">
				<h2>Deposit Schedule for Cashflow</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-pos/03_deposit_schedule.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   Depending on who your issuing bank is, we can connect you with an optimized depository channel that meets your expectations exactly.  This could be an advantage for you, it will boil down to rate cost and your cashflow expectations at the end of the day.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 3 -->	
			<!-- Start 4 -->
			<div class="row title text-center margin-around">
				<h2>All Payment Networks</h2>
			</div>
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							With Merchantocracy you can process on the Visa, Mastercard, American Experience and Interac payment networks.  Our terminal products are NFC enabled that can be customized for EMV Chip & PIN, Swipe and Contactless (Tap).
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-pos/02_customizable_settings.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 4 -->
			<!-- Start 5 -->
			<div class="row title text-center margin-around">
				<h2>Secure: Fraud Prevention</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-pos/03_deposit_schedule.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   Merchantocracy Terminals use state of the art security technologies that are all certified with the EMV and PCI Compliance keys. Our software uses the latest cryptographic schemes with future-proof key length.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 5 -->	
			<!-- Start 6 -->
			<div class="row title text-center margin-around">
				<h2>Maximized Network Availability</h2>
			</div>
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							We provide a full spectrum of wireless connectivity features in our POS Software, including 3G, GPRS, Dual SIM, Bluetooth and WiFi data services.  Our terminals offer the widest touch point of connectivity while optimzing communication costs.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-pos/02_customizable_settings.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 6 -->
			<!-- Start 7 -->
			<div class="row title text-center margin-around">
				<h2>User Friendly & Intuititive Interface</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-pos/03_deposit_schedule.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   Our hardware and software interfaces are designed intuitively, featuring powerful mutimedia capabilities and a large 3.5 inch touch screen.  Our terminal partners provide a best in class user experience thanks to a rich interface enabling business apps.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 7 -->	
	 </div>
	</section>
	<section class="section section-bot-padded section-transparent">
	</section>
	<section class="section section-slick-padded call-to-action">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="drk regular">Limited Time Offer</h3>
							<h4 class="drk regular">Sign up today and receive a 10% discount on your Point of Sale solution when you enrol before July 2018</h4>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="text-center">
							<h3 class="btn btn-xmain-xl"><a href="mailto:sales@merchantocracy.com" class="white" data-toggle="modal" data-target="#modal1">Ask us about our Free Merchant Program</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>