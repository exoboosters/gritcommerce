<!DOCTYPE html>
<html lang="en">
<!--  ---------------------------------------------------------------------------  -->
<!--  Handcoded with <3 in Toronto by exoboosters.tech                             -->
<!--  Date developed : Feb 2018                                                    -->
<!--  Author : Ramesh Vishveshwar                                                  -->
<!--  ---------------------------------------------------------------------------  -->
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<meta name="description" content="<?php echo $meta; ?>" />
	<meta name="keywords" content="merchants, payment, invoice, terminals" />
	<meta name="author" content="" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->

	<link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicons/favicon-16x16.png">
	<link rel="manifest" href="/img/favicons/site.webmanifest">
	<link rel="mask-icon" href="/img/favicons/safari-pinned-tab.svg" color="#6a2c90">
	<link rel="shortcut icon" href="/img/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#eeeeee">
	<meta name="msapplication-config" content="/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">

	<!-- End Favicons -->

	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="/css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="/css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="/css/animate.css">
	<!-- Body Font -->
	<link rel="stylesheet" type="text/css" href="/css/font.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="/fonts/font-awesome-4.1.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="/fonts/eleganticons/et-icons.css">
	<!-- Page Style -->
	<?php include 'css/css.php'; ?>
	
	<!-- Crisp Code -->
	<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="160938e6-7043-4d5d-a8a6-cc927cfdbed4";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>

<body>
	<div class="preloader">
		<img src="/img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="<?php echo $home_logo;?>" data-active-url="/img/logo-active.png" alt=""></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/">Home</a></li>
            <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Online Payments <span class="caret"></span></a>
				<ul class="dropdown-menu dropdown-content">
					<li><span><a href="/solutions/online-store" class="btn btn-small"><i class="mtcy mtcyonline-store"></i>Online Store</a></span></li>
					<li><span><a href="/solutions/invoicing" class="btn btn-small"><i class="mtcy mtcyinvoicing"></i>Invoicing</a></span></li>
					<li><span><a href="/solutions/virtual-terminals" class="btn btn-small"><i class="mtcy mtcyvirt-terminals"></i>Virtual Terminals</a></span></li>
					<li><span><a href="/solutions/web-integrations" class="btn btn-small"><i class="mtcy mtcyweb-integrations"></i>Website Integrations</a></span></li>
					<li><span><a href="/solutions/recurring-billing" class="btn btn-small"><i class="mtcy mtcyrecur-billing"></i>Recurring Billing</a></span></li>
				</ul>
            </li>
			<li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Offline Payments <span class="caret"></span></a>
				<ul class="dropdown-menu dropdown-content">
					<li><span><a href="/solutions/retail-pos" class="btn btn-small"><i class="mtcy  mtcyretail-pos"></i>Retail POS</a></span></li>
					<li><span><a href="/solutions/pos" class="btn btn-small"><i class="mtcy mtcypos"></i>Point of Sale</a></span></li>
				</ul>
            </li>
			<li><a href="/solutions/lower-rates">Rates</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#" data-toggle="modal" data-target="#modal2" class="btn btn-xmain">Login</a></li>
			<li><a href="#" data-toggle="modal" data-target="#modal1" class="btn btn-main">Follow Up</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
	