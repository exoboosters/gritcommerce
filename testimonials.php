	<section id="testimonials" class="section">
		<div class="container">
			<div class="row title text-center margin-around">
				<h2 class="white">Testimonials</h2>
				<h4 class="light white margin-around">See what our customers are saying</h4>
			</div>
			<div class="row title text-center">
				<h2>
					&nbsp;
				</h2>
			</div>
			<!-- Start Testimonial 1 -->
			<div class="row white no-margin">
				<div class="col-md-6 text-center box-main"><img src="img/testimonials/hull-n-hull.jpg" alt="" class="img-responsive center-block"></div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6 hidden-xs hidden-sm"><img src="img/testimonials/hull-n-hull-logo.jpg" alt="" class="img-responsive center-block"></div>
									  <div class="col-md-6 hidden-xs hidden-sm"><img src="img/testimonials/hull-n-hull-house.jpg" alt="" class="img-responsive center-block"></div>
					</div>
					<div class="row">
						 <div class="col-md-12" style="padding: 15px; line-height: 2em; ">
							<img src="img/testimonials/hull-n-hull-text.jpg" alt="" class="img-responsive center-block">
						 </div>
					</div>
				</div>
			</div>
			<!-- End T1 -->
			<div class="row title text-center">
				<h2>
					&nbsp;
				</h2>
			</div>
			<!-- Testimonial 2 -->
			<div class="row white no-margin">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6 hidden-xs hidden-sm"><img src="img/testimonials/collected-joy-sign.jpg" alt="" class="img-responsive center-block"></div>
						<div class="col-md-6 hidden-xs hidden-sm"><img src="img/testimonials/collected-joy-shop.jpg" alt="" class="img-responsive center-block"></div>
					</div>
					<div class="row">
						<div class="col-md-12" style="padding: 15px; line-height: 2em; ">
							<img src="img/testimonials/collected-joy-text.jpg" alt="" class="img-responsive center-block">
						</div>
					</div>
				</div>
				<div class="col-md-6 text-center box-main"><img src="img/testimonials/collected-joy.jpg" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End T2 -->
			<div class="row title text-center">
				<h2>
					&nbsp;
				</h2>
			</div>
		</div>
	</section>