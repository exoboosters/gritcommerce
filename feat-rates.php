	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="light white">The Secret Is Joining The Right Rate Program</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl"><a href="mailto:sales@merchantocracy.com" class="white">Start 90-day Trial Now</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section section-slick-padded blue-bg">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="drker light">Optimized pricing to meet yours and your customer needs</h3>
							<h4 class="drker light">All pricing is negotiable, speak to one of our experts to design the perfect program witht the lowest rates for you</h4>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials" class="section">
		<div class="container">
			<!-- Start 1 -->
			<!--<div class="row title text-center margin-around">
				<h2>Card Network Rates Overview</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-rates/000-lower-rates.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   There are no shortage of credit card choices for consumers, when you think about the growing number of issuing banks and loyalty and reward programs that are designed to differentiate card programs from what other banks are offering, you can understand why there are so many different rates to deal with. Please find an overview of our rates offered broken down by card network. 
						   </h5>
						</div>
					</div>
				</div>
			</div>-->
			<!-- End 1 -->	
			<div class="row title text-center margin-around">
				<h2>Start Collecting Visa Credit Card</h2>
			</div>
			<!-- Start 2 -->
			<div class="row drk-blue no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; ">
						<table class="table table-dark">
							<thead>
								<tr>
									<th scope="col">Rate Slab</th>
									<th scope="col">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Standardized Rate</td>
									<td>1.62%</td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 offset-3">
							<span class="fine-print">As low as 1.18% for high volume clients</span>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-rates/001-visa.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 2 -->
			<!-- Start 3 -->
			<div class="row title text-center margin-around">
				<h2>Start Collecting Mastercard Credit Card</h2>
			</div>
			<div class="row drk-blue no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-rates/002-mastercard.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; ">
						<table class="table table-dark">
							<thead>
								<tr>
									<th scope="col">Rate Slab</th>
									<th scope="col">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Standardized Rate</td>
									<td>1.64%</td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 offset-3">
							<span class="fine-print">As low as 1.2% for high volume clients</span>
						</div>
					</div>
				</div>
			</div>
			<!-- End 3 -->	
			<div class="row title text-center margin-around">
				<h2>Start Collecting Amex Blue Credit Card</h2>
			</div>
			<!-- Start 4 -->
			<div class="row drk-blue no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; ">
						<table class="table table-dark">
							<thead>
								<tr>
									<th scope="col">Rate Slab</th>
									<th scope="col">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Standardized Rate</td>
									<td>2.00%</td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 offset-3">
							<span class="fine-print">As low as 1.75% for high volume clients</span>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-rates/003-amex.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 4 -->
			<!-- Start 5 -->
			<div class="row title text-center margin-around">
				<h2>Per Transaction fees</h2>
			</div>
			<div class="row drk-blue no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-rates/004-interac.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-6 offset-3">
							<span class="fine-print">Above pricing may come down given higher volumes and remove it from the bottom of this particular section.</span>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; ">
						<table class="table table-dark">
							<thead>
								<tr>
									<th scope="col">Pricing Starting At:</th>
									<th scope="col">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Interac Debit Per Transaction</td>
									<td>$0.10</td>
								</tr>
								<tr>
									<td>Interac FLASH Per Transaction</td>
									<td>$0.15</td>
								</tr>
								<tr>
									<td>VISA Per Transaction</td>
									<td>$0.08</td>
								</tr>
								<tr>
									<td>MC Per Transaction</td>
									<td>$0.08</td>
								</tr>
								<tr>
									<td>AMEX Per Transaction</td>
									<td>$0.08</td>
								</tr>
								<tr>
									<td>Admin Transaction Fees</td>
									<td>$0.10</td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
					
				</div>
			</div>
			<!-- End 5 -->	
			<div class="row title text-center margin-around">
				<h2>Low monthly maintenance</h2>
			</div>
			<!-- Start 6 -->
			<div class="row drk-blue no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; ">
						<table class="table table-dark">
							<thead>
								<tr>
									<th scope="col">Monthly Charges starting at</th>
									<th scope="col">Rate</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Monthly Account maintenance</td>
									<td>$10.00</td>
								</tr>
								<tr>
									<td><em>(Optional)</em> POS Insurance & Guarantee</td>
									<td>$5.00</td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-rates/000-lower-rates.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 6 -->
	 </div>
	</section>
	<section class="section section-bot-padded section-transparent">
	</section>
	<section class="section section-slick-padded call-to-action">
		<div class="cut cut-top-lb"></div>
		<!--
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="item text-center">
						<h2 class="drk-blue light">Limited time offer</h3>
						<h4 class="drk-blue light">Receive 25% Discount On Your Hardware If You Enrol Before March 2018 - Sign Up today</h4>
					</div>
				</div>
			</div>
		</div>
		-->
	</section>