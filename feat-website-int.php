	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="light white">Effortlessly Partner With Any 3rd Party Web Applications</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl"><a href="mailto:sales@merchantocracy.com" class="white">Email Us To Set Up Trial Offer</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials" class="section">
		<div class="container">
			<!-- Start 1 -->
			<div class="row title text-center margin-around">
				<h2>Partner With Anyone</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-website-int/001_partner_with_anyone.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   Commerce and technology are expanding exponentially, so to be competitive in today's marketplace must involve partners. We work with external dev teams to help connect your payment processing solution to ours so that your business continues to generate cashflow and thrive.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 1 -->	
			<div class="row title text-center margin-around">
				<h2>3rd Party Shopping Cart API's</h2>
			</div>
			<!-- Start 2 -->
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							We partner with most 3rd party checkouts today, as API's are now made available by most companies that provide a payment checkout gateway.  As a result, we will work with your company and the 3rd party partner to increase the end user satisfaction and better overall customer eperience.  We will help you manage and own the technical integration queries to provide regular reporting, so that you can keep your eye on your business.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-website-int/002_3rd_party_shopping.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 2 -->
			<!-- Start 3 -->
			<div class="row title text-center margin-around">
				<h2>3rd Party Accounting API's</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-website-int/003_3rd_party_acct.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   We have developed API's for accounting software to help you process payments and pull transaction data into your accounting system. You can also retrieve online reports and process refund charges, speak with one our technical representatives to explore capabilities in more detail.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 3 -->	
			<div class="row title text-center margin-around">
				<h2>Customized Developer API Kit</h2>
			</div>
			<!-- Start 4 -->
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							Not all partners offer a cookie cutter API, we will provide you with an in-house developer to make the integration seamless.  Depending on the size of the project, it may involve quoting the actual cost and mapping out how to pay for it over time, so that you don't incur any major upfront cost.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-website-int/004_custom_developer_api.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 4 -->
	 </div>
	</section>
	<section class="section section-bot-padded section-transparent">
	</section>
	<section class="section section-slick-padded call-to-action">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="drk regular">Limited Time Offer</h3>
							<h4 class="drk regular">Sign up today and receive a 10% discount on your Website Integration consulting work when you enrol before July 2018</h4>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="text-center">
							<h3 class="btn btn-xmain-xl"><a href="mailto:sales@merchantocracy.com" class="white" data-toggle="modal" data-target="#modal1">Ask us about our Free Merchant Program</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>