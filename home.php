	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3 class="light white">Provide the payment experience your customers want</h3>
							<h1 class="white typed">with Merchantocracy</h1>
							<span class="typed-cursor">|</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<a href="#" data-toggle="modal" data-target="#modal1"><h3 class="btn btn-main-xl">Sign up Today</h3></a>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section section-slick-padded blue-bg">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="white light">Unleash your Cashflow Potential.</h3>
							<h4 class="white light">Start by Making it easy to collect payment.</h4>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section>
			<div class="cut cut-bottom-lb"></div>
	</section>
	
	<!-- Solutions -->
	<?php include 'solutions.php'; ?>
	<!-- End Solutions -->
	
	<!-- Testimonials -->
	<?php include 'testimonials.php'; ?>
	<!-- End Testimonials -->
	
	<!-- Tweets -->
	<?php include 'tweets.php'; ?>
	<!-- End Tweets -->