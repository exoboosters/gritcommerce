	<style>

		@import url(http://fonts.googleapis.com/css?family=Montserrat:300,400,500,700);
		body {
			font-family: 'Montserrat', 'Helvetica Neue', 'Segoe UI', Helvetica, Arial, sans-serif;
			font-size: 14px;
			overflow-x: hidden;
			color: #2a3237;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
		}

		.parallax {
			position: absolute;
			overflow: hidden;
			width: 100%;
			-webkit-transform: translateZ(0);
			-ms-transform: translateZ(0);
			transform: translateZ(0);
		}

		.parallax img {
			width: 100%;
			height: 100%;
		}
		/* Preloader */

		.preloader {
			position: fixed;
			z-index: 9999;
			width: 100%;
			height: 100%;
			background-color: white;
		}

		.preloader img {
			position: absolute;
			top: calc(50% - 32px);
			left: calc(50% - 32px);
		}

		.preloader div {
			display: none;
		}

		/* Typography */

		p {
			font-size: 18px;
			line-height: 1.5;
			color: #a9b1b8;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		h6 {
			font-family: 'Montserrat', 'Avenir Next', 'Helvetica Neue', 'Segoe UI', Helvetica, Arial, sans-serif;
			position: relative;
			margin: 10px 0;
		}

		h1 {
			font-size: 60px;
		}

		h2 {
			font-size: 48px;
		}

		h3 {
			font-size: 30px;
		}

		h4 {
			font-size: 24px;
		}

		h5 {
			font-size: 18px;
		}

		h6 {
			font-size: 16px;
		}
		
		.fine-print {
			font-size: 10px;
		}

		ul.white-list {
			padding: 0;
			list-style-type: none;
		}

		ul.white-list li {
			font-size: 18px;
			margin: 10px 0;
			color: #fff;
		}

		ul.white-list li:before {
			content: ' ';
			position: relative;
			top: -3px;
			display: inline-block;
			width: 6px;
			height: 6px;
			margin-right: 15px;
			background: white;
		}

		header {
			position: relative;
			width: 100%;
			height: 100%
			color: white;
			background: url('../img/headers/<?php echo $css_elements; ?>.jpg');
			background-size: cover;
			background-position: center;
			position: relative;
		} 
		  header:before {
			content: '';
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			background-image: linear-gradient(to bottom right,<?php echo $acce_col . ',' . $darker; ?>);
			opacity: .7; 
		  }

		header .container {
			height: 100%;
		}

		header .table {
			display: table;
			height: 100%;
		}


		header .header-text {
			display: table-cell;
			text-align: center;
			vertical-align: middle;
			color: white;
		}

		header .typed {
			display: inline-block;
			margin: 0;
		}

		header .typed-cursor {
			font-size: 60px;
			display: inline-block;
			margin: 0 10px;
			color: <?php echo $prim_col;?>;
			-webkit-animation-name: flash;
			animation-name: flash;
			-webkit-animation-duration: 1s;
			animation-duration: 1s;
			-webkit-animation-iteration-count: infinite;
			animation-iteration-count: infinite;
		}

		a {
			text-decoration: none;
			color: <?php echo $prim_col;?>;
			-webkit-transition: all 0.3s ease;
			transition: all 0.3s ease;
		}
		
		a.foot-link {
			text-decoration: none;
			color: #fff;
			-webkit-transition: all 0.3s ease;
			transition: all 0.3s ease;
		}
		/* Navigation Bar ( Navbar ) */

		nav.navbar {
			position: fixed;
			top: 0px;
			padding: 25px 0;
			z-index: 9500;
			width: 100%;
			width: 100vw;
			-webkit-transition: all 0.3s ease;
			transition: all 0.3s ease;
			background-color: rgba(255, 255, 255, 0.95);
		}

		nav.navbar .navbar-nav li.active a:not(.btn) {
			color: <?php echo $darker;?> !important;
		}
		
		nav.navbar .navbar-nav li a:not(.btn) {
			color: <?php echo $darker;?> !important;
		}
		

		nav.navbar-fixed-top {
			z-index: 9499;
			top: 0;
			padding: 25px 0;
			opacity: 0;
			background: white;
			box-shadow: 0px 4px 3px rgba(0, 0, 0, 0.05);
		}

		nav.navbar-fixed-top .navbar-nav > li > a:not(.btn) {
			color: #bbb;
		}
		
		.icon-bar {
			background: #000;
		}
	
		.main-nav {
			position: inherit;
		}

		/* Buttons */

		.btn {
			font-size: 18px;
			display: inline-block;
			padding: 15px 30px;
			color: white;
			border: 2px solid transparent;
			border-radius: 2px;
			background: transparent;
			-webkit-transition: all 0.3s ease;
			transition: all 0.3s ease;
		}

		.btn:hover,
		.btn:focus {
			color: white;
		}

		.btn-small {
			font-size: 14px;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		
		.btn.btn-main-xl {
			background: <?php echo $prim_col;?>;
			font-size: 28px;
		}

		.btn.btn-main-xl:hover {
			background: #48514f;
		}
		
		.btn.btn-xmain-xl {
			background: #48514f;
			font-size: 28px;
		}

		.btn.btn-xmain-xl:hover {
			background: <?php echo $darker;?>;
			color: #fff;
		}

		.btn.btn-main {
			background: <?php echo $prim_col;?>;
		}

		.btn.btn-main:hover {
			background: #48514f;
		}

		.btn.btn-xmain {
			color: <?php echo $darker;?>;
			border-color: <?php echo $prim_col;?>;
			background: transparent;
		}

		.btn.btn-xmain:hover {
			background: <?php echo $prim_col;?>;
			color:#fff;
		}

		.btn.btn-blue-fill:hover {
			color: <?php echo $prim_col;?>;
			border-color: <?php echo $prim_col;?>;
			background: #ffffff;
		}

		.btn.btn-blue-fill {
			color: white;
			background: <?php echo $prim_col;?>;
		}

		.btn.btn-white-fill {
			color: #fff;
			border-color: #fff;
			background: transparent;
		}

		.btn.btn-white-fill:hover {
			color: <?php echo $prim_col;?>;
			background: #fff;
		}

		.btn.btn-gray-fill {
			color: #fff;
			border-color: #fff;
			background: transparent;
		}

		.btn.btn-gray-fill:hover {
			border-color: #bbb;
			background: #bbb;
		}

		/* Blink Cursor */

		.blink {
			position: relative;
			top: 4px;
			display: inline-block;
			width: 4px;
			height: 50px;
			height: 5vh;
			margin: 0 10px;
		}

		.navbar {
			top: 50px;
		}

		.container {
			position: relative;
			z-index: 1;
		}
		/* Sections */
		section {
			position: relative;
		}

		.section {
			padding: 40px 0;
			background: #31b9ff;
		}

		.section-padded {
			padding: 140px 0 40px;
		}
		
		.section-transparent {
			background: <?php echo $cont_en_col;?>;
		}
		
		.section-bot-padded {
			padding: 80px 0 20px;
		}

		.section-slick-padded {
			padding: 80px 0 80px;
		}

		.cut-top-lb {
			content: ' ';
			position: absolute;
			z-index: 1;
			top: -80px;
			left: 0;
			width: 0;
			height: 0;
			border-top: 80px solid transparent;
			border-right: 30px solid <?php echo $prim_col;?>;
		}

		.cut-bottom-lb {
			content: ' ';
			position: absolute;
			z-index: 1;
			bottom: -80px;
			left: 0;
			width: 0;
			height: 0;
			border-bottom: 80px solid transparent;
			border-left: 30px solid <?php echo $prim_col;?>;
		}

		.cut-top {
			content: ' ';
			position: absolute;
			z-index: 1;
			top: -80px;
			left: 0;
			width: 0;
			height: 0;
			border-top: 80px solid transparent;
			border-right: 30px solid white;
		}

		.cut-bottom {
			content: ' ';
			position: absolute;
			z-index: 1;
			bottom: -80px;
			left: 0;
			width: 0;
			height: 0;
			border-bottom: 80px solid transparent;
			border-left: 30px solid white;
		}

		.hidden-col {
			display:none;
		}

		.intro-tables {
			top: -150px;
			position: relative;
		}

		.intro-table {
			-webkit-background-size: cover;
			background-size: cover;
			background-repeat: repeat;
			background-position: 0% 0%;
		}

		.intro-table-first {
			background-image: url('../img/table-1.jpg');
		}

		.intro-table-hover {
			-webkit-transition: background-image 0.3s ease, background-position 0.3s;
			transition: background-image 0.3s ease, background-position 0.3s;
			background-image: url('../img/table-2.jpg');
		}

		.intro-table-hover h4 {
			-webkit-transform: translateY(170px);
			transform: translateY(170px);
			-webkit-transition: -webkit-transform 0.3s;
			transition: transform 0.3s;
		}

		.intro-table-hover:hover {
			background-image: url('../img/table-2-hover.jpg');
			background-position: 50% 50%;
		}

		.intro-table-third {
			background-image: url('../img/table-3.jpg');
		}

		.intro-table-hover .expand {
			margin: 30px;
			margin-top: 120px;
			opacity: 0;
			-webkit-transition: -webkit-transform 0.3s, opacity 0.3s;
			transition: transform 0.3s ease, opacity 0.3s;
			-webkit-transform: scale(0.6);
			-ms-transform: scale(0.6);
			transform: scale(0.6);
		}

		.intro-table-hover:hover h4 {
			-webkit-transform: translateY(0);
			transform: translateY(0);
		}

		.intro-table-hover:hover .expand {
			opacity: 1;
			-webkit-transform: scale(1);
			-ms-transform: scale(1);
			transform: scale(1);
		}

		.intro-table-hover .hide-hover {
			-webkit-transition: opacity 0.3s ease;
			transition: opacity 0.3s ease;
		}

		.intro-table-hover:hover .hide-hover {
			opacity: 0;
		}

		.intro-tables .intro-table {
			position: relative;
			width: 100%;
			height: 300px;
			margin: 20px 0;
		}

		.intro-tables .intro-table .heading {
			margin: 0;
			padding: 30px;
		}

		.intro-tables .intro-table .small-heading {
			margin: 0;
			padding: 0 30px;
		}

		.intro-tables .intro-table .bottom {
			position: absolute;
			bottom: 0;
		}

		.intro-tables .intro-table .owl-schedule .schedule-row {
			padding: 10px 30px;
			color: white;
			transition: all 0.3s ease;
		}

		.owl-schedule .schedule-row:not(:last-child) {
			border-bottom: 1px solid rgba(255, 255, 255, 0.4);
		}

		.owl-testimonials .author {
			margin-top: 50px;
		}

		.ripple-effect {
			position: absolute;
			width: 50px;
			height: 50px;
			border-radius: 50%;
			background: white;
			-webkit-animation: ripple-animation 2s;
			animation: ripple-animation 2s;
		}

		@-webkit-keyframes ripple-animation {
			from {
				opacity: 0.2;
				-webkit-transform: scale(1);
				transform: scale(1);
			}
			to {
				opacity: 0;
				-webkit-transform: scale(100);
				transform: scale(100);
			}
		}

		@keyframes ripple-animation {
			from {
				opacity: 0.2;
				-webkit-transform: scale(1);
				transform: scale(1);
			}
			to {
				opacity: 0;
				-webkit-transform: scale(100);
				transform: scale(100);
			}
		}

		.services {
			margin: 40px 0;
		}

		.service {
			width: 100%;
			height: 320px;
			margin: 80px 0;
			text-align: center;
			border: 1px solid #fff;
			-webkit-transition: all 0.3s ease;
			transition: all 0.3s ease;
		}

		.service .icon-holder {
			position: relative;
			top: 40px;
			display: inline-block;
			margin-bottom: 40px;
			padding: 10px;
			background: <?php echo $acce_col;?>;
			-webkit-transition: all 0.3s ease;
			transition: all 0.3s ease;
		}

		.service .heading {
			position: relative;
			color:#fff;
			top: 80px;
			padding: 10px;
			-webkit-transition: all 600ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
			transition: all 600ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
		}

		.red-icon {
			color:#e31b23;
			opacity:0.7;
		}

		.service .icon-holder > img.icon {
			width: 80px;
		}

		.service:hover {
			border-color: <?php echo $prim_col;?>;
		}

		.service:hover .icon-holder {
			top: -50px;
		}

		.service:hover .heading {
			top: -50px;
		}

		.service .description {
			width: 80%;
			margin: 0 auto;
			opacity: 0;
			color:#fff;
			-webkit-transition: all 600ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
			transition: all 600ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
			-webkit-transform: scale(0);
			-ms-transform: scale(0);
			transform: scale(0);
		}

		.service:hover .description {
			opacity: 1;
			-webkit-transform: scale(1);
			-ms-transform: scale(1);
			transform: scale(1);
		}

		.team {
			margin: 80px 0;
			padding-bottom: 60px;
			background: white;
			box-shadow: 0 2px 3px rgba(0, 0, 0, 0.07);
		}

		.team .cover .overlay {
			height: 250px;
			padding-top: 60px;
			opacity: 0;
			background: rgba(0, 168, 255, 0.9);
			-webkit-transition: opacity 0.45s ease;
			transition: opacity 0.45s ease;
		}

		.team:hover .cover .overlay {
			opacity: 1;
		}

		.team .avatar {
			position: relative;
			z-index: 2;
			margin-top: -60px;
			border-radius: 50%;
		}

		.team .title {
			margin: 50px 0;
		}

		/* Testimonials */

		#testimonials {
			background: <?php echo $cont_st_col;?>; /* Old browsers */
		  background: -moz-linear-gradient(top, <?php echo $cont_st_col;?> 0%, <?php echo $cont_en_col;?> 100%); /* FF3.6-15 */
		  background: -webkit-linear-gradient(top, <?php echo $cont_st_col;?> 0%,<?php echo $cont_en_col;?> 100%); /* Chrome10-25,Safari5.1-6 */
		  background: linear-gradient(to bottom, <?php echo $cont_st_col;?> 0%,<?php echo $cont_en_col;?> 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $cont_st_col;?>', endColorstr='<?php echo $cont_en_col;?>',GradientType=0 ); /* IE6-9 */
			-webkit-background-size: cover;
			background-size: cover;
			-webkit-transition: background-image 0.6s linear 0.3s;
			transition: background-image 0.6s linear 0.3s;
		}

		/* Pricing */

		#pricing {
			background: #45484d; /* Old browsers */
		  background: -moz-linear-gradient(top, #45484d 0%, #000000 100%); /* FF3.6-15 */
		  background: -webkit-linear-gradient(top, #45484d 0%,#000000 100%); /* Chrome10-25,Safari5.1-6 */
		  background: linear-gradient(to bottom, #45484d 0%,#000000 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#45484d', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
			-webkit-background-size: cover;
			background-size: cover;
			-webkit-transition: background-image 0.6s linear 0.3s;
			transition: background-image 0.6s linear 0.3s;
		}

		.owl-pricing img {
			width: 100%;
		}

		.owl-pricing,
		.pricings {
			margin-top: 100px;
			margin-bottom: 100px;
		}

		.pricing {
			position: relative;
			width: 100%;
		}

		.pricings .pricing .box-main,
		.pricings .pricing .box-second {
			position: relative;
			left: 25%;
			display: inline-block;
			width: 50%;
			height: 300px;
			padding: 50px 40px;
			background: #bbb;
			-webkit-transition: -webkit-transform 0.3s, background-image 0.3s, opacity 0.3s;
			transition: transform 0.3s, background-image 0.3s, opacity 0.3s;
			-webkit-backface-visibility: hidden;
			backface-visibility: hidden;
		}

		.pricings .pricing .box-main {
			z-index: 10;
			padding-top: 40px;
		}

		.pricings .pricing .box-main:not(.active) {
			cursor: pointer;
		}

		.pricings .pricing .box-main .info-icon {
			font-size: 14px;
			position: absolute;
			top: 20px;
			right: 20px;
			pointer-events: none;
			opacity: 0;
			color: #fff;
			-webkit-transition: opacity 0.3s;
			transition: opacity 0.3s;
		}

		.pricings .pricing .box-main:not(.active) .info-icon {
			opacity: 1;
			-webkit-transition-delay: 0.2s;
			transition-delay: 0.2s;
		}

		.pricings .pricing .box-main:not(.active):hover {
			background: #afafaf;
		}

		.pricings .pricing .box-main.active {
			background: <?php echo $prim_col;?>;
			-webkit-transform: translateX(-99%);
			-ms-transform: translateX(-99%);
			transform: translateX(-99%);
		}

		.pricings .pricing .box-second {
			position: absolute;
			top: 0;
			right: 0% !important;
			left: auto;
			opacity: 0;
			background: #afafaf;
		}

		.pricings .pricing .box-second.active {
			opacity: 1;
			background: <?php echo $prim_col;?>;
		}

		.pricings .pricing.active .box-main,
		.pricings .pricing .box-second {
			background: <?php echo $prim_col;?>;
		}

		.pricings .pricing .box-main a.btn {
			margin-top: 50px;
		}

		.owl-twitter i.icon {
			font-size: 36px;
			margin-bottom: 60px;
			color: white;
		}
		/* Footer */

		/* Hopefully clever part on how to apply gradients for the footer */

		footer .letsaddcolor {
			padding: 140px 40px 40px 40px;
			width:100%;
			height: 100%;
			background: 
			/* top, transparent red */ 
			linear-gradient(
			  to bottom right,
			  rgba(<?php echo $acce_rgb?>, 0.6),
			  rgba(<?php echo $darker_rgb?>, 0.6)
			),
			/* bottom, image */
			url('../img/leather-nunchuck.png');
			background-repeat: repeat;
			width: 100%;
		}

		footer .trial-button {
			overflow: hidden !important;
			margin: 40px 0;
		}

		footer .open-blink {
			content: ' ';
			position: relative;
			display: inline-block;
			width: 14px;
			height: 14px;
			margin: 0 20px;
			border-radius: 50%;
			background-color: #4caf50;
			-webkit-animation-name: flash;
			animation-name: flash;
			-webkit-animation-duration: 1s;
			animation-duration: 1s;
			-webkit-animation-iteration-count: infinite;
			animation-iteration-count: infinite;
		}

		footer .open-blink:before {
			content: ' ';
			position: absolute;
			top: -8px;
			left: -8px;
			display: inline-block;
			width: 30px;
			height: 30px;
			opacity: 0.1;
			border-radius: 50%;
			background-color: #4caf50;
		}

		footer .opening-hours {
			margin-top: 30px;
			margin-bottom: 30px;
		}

		footer .bottom-footer {
			margin-top: 150px;
		}

		footer .social-footer {
			padding: 0;
			list-style: none;
		}

		footer .social-footer li {
			display: inline-block;
			margin: 0 10px;
		}

		footer .social-footer li a {
			font-size: 24px;
			color: #fff;
		}

		footer .social-footer li:hover a {
			color: <?php echo $prim_col;?>;
		}
		/* Form Control */

		.form-control {
			font-size: 18px;
			position: relative;
			left: 0;
			height: auto;
			padding: 10px 15px;
			border: 1px;
			border-radius: 0;
			box-shadow: 0;
		}

		.form-control.form-white {
			color: #fff;
			border: 2px solid white;
			background: transparent;
			-webkit-transition: background-color 0.3s;
			transition: background-color 0.3s;
		}

		.form-control.form-white::-webkit-input-placeholder {
			/* WebKit browsers */
			color: #fff;
		}

		.form-control.form-white:-moz-placeholder {
			opacity: 1;
			/* Mozilla Firefox 4 to 18 */
			color: #fff;
		}

		.form-control.form-white::-moz-placeholder {
			opacity: 1;
			/* Mozilla Firefox 19+ */
			color: #fff;
		}

		.form-control.form-white:-ms-input-placeholder {
			/* Internet Explorer 10+ */
			color: #fff;
		}

		.form-control.form-white:focus {
			background: rgba(255,255,255,0.2);
		}

		/* Popup */
		.modal {
			padding: 0 25px !important;
		}

		.modal-dialog {
			width: 100%;
			max-width: 80vw;
			margin: 0 auto;
		}

		.modal-signin {
			width: 80%;
			max-width: 50vw;
			margin: 0 auto;
		}

		.modal-popup {
			position: relative;
			padding: 25px 15px;
			text-align: center;
			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/<?php echo $acce_col;?>+0,2b5448+100 */
			background: <?php echo $acce_col;?>; /* Old browsers */
			background: -moz-linear-gradient(-45deg, <?php echo $acce_col;?> 0%, <?php echo $darker;?> 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(-45deg, <?php echo $acce_col;?> 0%,<?php echo $darker;?> 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(135deg, <?php echo $acce_col;?> 0%,<?php echo $darker;?> 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='<?php echo $acce_col;?>', endColorstr='<?php echo $darker;?>',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
			box-shadow: none;
			border-radius: 2px;
		}

		.modal-popup a.close-link {
			font-size: 22px;
			position: absolute;
			top: 20px;
			right: 30px;
			color: #fff;
		}

		.popup-form {
			width: 100%;
			max-width: 70vw;
			margin: 30px auto;
		}

		.popup-form .form-control {
			margin: 20px 0;
		}

		.popup-form .form-control.dropdownf {
			text-align: left;
		}

		.popup-form .form-control.dropdownf:after {
			content: '\f0d7';
			font-family: 'FontAwesome';
			display: inline-block;
			float: right;
			color: white;
		}
		
		.popup-form .pull-right {
			float: right;
		}

		.popup-form .dropdownf .dropdown-menu {
			top: 65px;
			width: 100%;
			padding: 0;
			border: 2px solid white;
			border-top: 0;
			border-radius: 0;
			background: white;
			box-shadow: none;
		}

		.popup-form .dropdownf .dropdown-menu li {
			font-size: 16px;
			width: 100%;
			background: transparent;
		}

		.popup-form .dropdownf .dropdown-menu li a {
			width: 100%;
			padding: 15px 30px;
			color: <?php echo $acce_col;?>;
		}

		.popup-form .dropdownf .dropdown-menu li:hover a {
			color: #fff;
			background: <?php echo $acce_col;?>;
		}

		/* Checkbox */

		.checkbox-holder {
			white-space: nowrap;
		}

		.checkbox {
			position: relative;
			display: block;
		}

		.checkbox {
			position: relative;
		}

		.checkbox label:before {
			content: '';
			position: absolute;
			top: 0;
			left: 0;
			width: 20px;
			height: 20px;
			cursor: pointer;
			border: 2px solid white;
			background: transparent;
			-webkit-transition: background-color 0.3s;
			transition: background-color 0.3s;
		}

		.checkbox input[type=checkbox]:focus + label:before {
			background: rgba(255,255,255,0.2);
		}

		.checkbox label:after {
			content: '';
			position: absolute;
			top: 6px;
			left: 6px;
			width: 8px;
			height: 8px;
			opacity: 0;
			background: white;
			-webkit-transition: all 0.3s ease;
			transition: all 0.3s ease;
		}

		.checkbox input[type=checkbox] {
			opacity: 0;
			position: absolute;
			width: 0;
			height: 0;
		}

		.checkbox input[type=checkbox]:checked + label:after {
			opacity: 1;
		}

		.checkbox-holder span {
			position: relative;
			display: inline-block;
			margin: 0 0 0 10px;
			white-space: normal;
			color: #fff;
		}

		.btn.btn-submit {
			width: 100%;
			margin-top: 30px;
			color: <?php echo $acce_col;?>;
			border: 2px solid #fff;
			background: #fff;
		}

		.btn.btn-submit:focus {
			font-weight: bold;
		}

		.btn.btn-submit:hover {
			color: <?php echo $darker;?>;
			background: <?php echo $prim_col;?>;
		}

		/* Mobile Nav */
		.mobile-nav {
			position: fixed;
			z-index: 9999;
			top: 0;
			left: 0;
			display: table;
			width: 100%;
			height: 100%;
			text-align: center;
			opacity: 0;
			background: rgba(255, 255, 255, 1);
			-webkit-transition: all 600ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
			transition: all 600ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
			-webkit-transform: scale(0);
			-ms-transform: scale(0);
			transform: scale(0);
		}

		.mobile-nav.active {
			opacity: 1;
			-webkit-transform: scale(1);
			-ms-transform: scale(1);
			transform: scale(1);
			background: #fff;
		}

		.mobile-nav ul {
			display: table-cell;
			padding: 0;
			list-style: none;
			vertical-align: middle;
		}

		.mobile-nav ul li {
			margin: 25px 0;
		}

		.mobile-nav ul li a:not(.btn) {
			color: #aaa;
		}

		.mobile-nav a.close-link {
			font-size: 24px;
			position: absolute;
			bottom: 0px;
			left: calc(50% - 10px);
			left: 0;
			width: 100%;
			padding: 15px 0;
			color: #fff;
			background: <?php echo $prim_col;?>;
		}

		.row.title {
			padding: 0 20px;
		}

		.light {
			font-weight: 300;
		}

		.regular {
			font-weight: 400;
		}

		.bold {
			font-weight: bold;
		}
		/* Colors */

		.white {
			color: white;
		}

		.drk {
			color: #333;
		}
		
		.drk-blue {
			color: <?php echo $acce_col;?>;
		}
		
		.drker {
			color: <?php echo $darker;?>;
		}

		.light-white {
			color: rgba(255, 255, 255, 0.5);
		}

		.white-bg {
			background: white;
		}

		.gray-bg {
			background: #f7f7f7;
		}

		.blue {
			color: <?php echo $prim_col;?>;
		}

		.blue-bg {
			background: <?php echo $prim_col;?>;
		}

		.call-to-action {
			background: <?php echo $prim_col;?>;
		}

		.blue-twit-bg {
			background: #00aced;
		}

		.grey-bg {
			background: #F3C167;
		}

		.drk-blue-bg {
			background: <?php echo $acce_col;?>;
		}

		.muted {
			color: #989da0;
		}

		.margin-top {
			margin-top: 150px;
		}

		.margin-around {
			margin-top: 50px;
			margin-bottom: 50px;
		}

		@media(max-width:992px) {
			h1 {
				font-size: 36px;
			}
			h2 {
				font-size: 28px;
			}
			h3 {
				font-size: 24px;
			}
			h4 {
				font-size: 20px;
			}
			h5 {
				font-size: 16px;
			}
			h6 {
				font-size: 12px;
			}
			.section {
				padding: 30px 0;
			}
		}

		.dropdown {
			position: relative;
			display: inline-block;
		}

		.dropdown-content {
			display: none;
			position: absolute;
			background-color: rgba(<?php echo $acce_rgb?>, 0.9);
			min-width: 200px;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			padding: 5px 5px;
			z-index: 1;
			color: #fff;
			
		}

		.dropdown-content ul {
			list-style: none;
			
		}
		
		.dropdown-content a {
			color: #fff;
			line-height: 1.2em;
			padding-top: 0px;
			padding-bottom: 5px;
			margin-top: 5px;
			margin-left: -10px; 
		}
		
		.dropdown-content a:hover, 
		.dropdown-content a:focus {
			background-color: <?php echo $darker;?>;
			margin-left: 0px;
		}

		.dropdown-content a.current {
			background-color: <?php echo $darker;?>;
		}
		
		.nav > li > a:hover,
		.nav > li > a:focus {
		  text-decoration: none;
		  background-color: transparent;
		}

		.nav .open > a,
		.nav .open > a:hover,
		.nav .open > a:focus {
		  background-color: transparent;
		  border: 0px;
		}

		
		/* Media Queries */

		@media(max-width:991px) {
			.navbar {
				background-color: rgba(0,0,0,0.5);
			}
			.text-center-mobile {
				text-align: center !important;
			}
			.dropdown-content {
				display: none;
				position: absolute;
				background-color: rgba(<?php echo $acce_rgb?>, 0.9);
				min-width: 200px;
				box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
				padding: 5px 5px;
				z-index: 1;
				color: #fff;
				
			}
		}

		@media(max-width: 768px) {
			.navbar {
				background-color: rgba(0,0,0,0.5);
			}
			.dropdown-content {
				display: none;
				position: absolute;
				background-color: rgba(<?php echo $acce_rgb?>, 0.9);
				min-width: 200px;
				box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
				padding: 5px 5px;
				z-index: 1;
				color: #fff;
				
			}
			.pricing {
				margin-bottom: 30px;
			}
			.pricings .pricing .info-icon {
				display: none;
			}
			.pricings .pricing .box-main,
			.pricings .pricing .box-second {
				left: 0;
				width: 100%;
				padding: 50px 50px 0;
				text-align: left;
				background: <?php echo $prim_col;?>;
			}
			.pricings .pricing .box-main.active {
				background: <?php echo $prim_col;?>;
				-webkit-transform: translateX(0%);
				-ms-transform: translateX(0%);
				transform: translateX(0%);
			}
			.pricings .pricing .box-second {
				position: relative;
				opacity: 1;
			}
			.popup-form {
				width: 100%;
				margin: 60px auto;
			}
			.modal {
				padding: 0 10px !important;
			}
			.popup-form .form-control:not(.dropdown):focus {
				position: relative;
				padding-right: 30px;
				padding-left: 30px;
			}
		}

		@media(max-width: 400px) {
			header .typed-cursor {
				display: none;
			}
			.navbar {
				background-color: rgba(0,0,0,0.5);
			}
			.pricings .pricing .box-second {
				padding-top: 0;
			}
		}
		
		@media(min-width: 1368px) {
			.container {
				max-width: 1350px;
			}
		}
	</style>