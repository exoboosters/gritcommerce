	<footer>
		<div class="letsaddcolor">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Are you ready to transform your payment experience</h3>
					<!--<h5 class="light regular light-white">Transform your merchant experience</h5>-->
					<a href="mailto:sales@merchantocracy.com" class="btn btn-blue-fill ripple trial-button">Request Demo</a>
				</div>
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Have Questions? <span class="open-blink"></span></h3>
					<h6 class="light-white">Mon-Fri, 9am to 5pm EST.</h6>
					</h6>
					<div class="row opening-hours">
						<div class="col-sm-12 text-center-mobile">
							<h5 class="light-white light"><i class="info-icon icon_phone"></i>&nbsp;Talk to a human</h5>
							<h3 class="regular white">1.855.333.6084</h3>
						</div>
						<!--<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light"><i class="info-icon icon_chat"></i>&nbsp;Start a Live Chat</h5>
							<h3 class="regular white">Chat</h3>
						</div>-->
					</div>
				  <h3 class="white">Support</h3>
					<h6 class="light-white">24/7/365 here for you</h6>
					<div class="row opening-hours">
						<div class="col-sm-12 text-center-mobile">
							<h5 class="light-white light"><i class="info-icon icon_mail"></i>&nbsp;Email</h5>
							<h3 class="regular white">support@merchantocracy.com</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row bottom-footer text-center-mobile">
				<div class="col-sm-8">
					<p>&copy; 2011 - 2018 All Rights Reserved by <a href="https://merchantocracy.com" class="foot-link">Merchantocracy</a>&reg;</p>
				</div>
				<div class="col-sm-4 text-right text-center-mobile">
					<ul class="social-footer">
						<li><a href="https://www.facebook.com/Merchantocracy/" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://www.twitter.com/merchantocracy" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<li><a href="https://www.linkedin.com/company/merchantocracy-inc-/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="row text-center-mobile">
				<div class="col-sm-12">
					<p align="center">Handcrafted with <a href="https://exoboosters.tech" title="Designed & Maintained by Exoboosters.Tech" target="_blank"><i class="red-icon icon_heart"></i></a> in Canada</p>
				</div>
			</div>
		<!-- End container -->
		</div>
		<!-- End letsaddcolor -->
		</div>
	</footer>
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="/js/jquery-1.11.1.min.js"></script>
	<script src="/js/owl.carousel.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/wow.min.js"></script>
	<script src="/js/typewriter.js"></script>
	<script src="/js/jquery.onepagenav.js"></script>
	<script src="/js/main.js"></script>
	<!-- Google Analytics -->
	<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-30519836-1"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());  gtag('config', 'UA-30519836-1');
	</script>
</body>
<!--  ______           _                     _                 _            _      -->
<!-- |  ____|         | |                   | |               | |          | |     -->
<!-- | |__  __  _____ | |__   ___   ___  ___| |_ ___ _ __ ___ | |_ ___  ___| |__   -->
<!-- |  __| \ \/ / _ \| '_ \ / _ \ / _ \/ __| __/ _ \ '__/ __|| __/ _ \/ __| '_ \  -->
<!-- | |____ >  < (_) | |_) | (_) | (_) \__ \ ||  __/ |  \__ \| ||  __/ (__| | | | -->
<!-- |______/_/\_\___/|_.__/ \___/ \___/|___/\__\___|_|  |___(_)__\___|\___|_| |_| -->
<!--                                                                               -->
<!--                                                                               -->
</html>
