	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="light white">Things You Ought To Know About Processing Online Store Payments</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl"><a href="mailto:sales@merchantocracy.com" class="white">Email Us To Set Up Trial Offer</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials" class="section">
		<div class="container">
			<div class="row title text-center margin-around">
				<h2>Beautiful Online Store Experience</h2>
			</div>
			<!-- Start 1 -->
			<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-online-store/01_beautiful_online.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Operating an online store requires a heightened attention to user expereience and deisgn attributes. our Online Store Solutions offers store operators the ability to fully customize every button, every colour, every background fill and add customized icons that reflect your brand exactly.</h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 1 -->	
			<div class="row title text-center margin-around">
				<h2>Hosted or 3rd Party Hosted Solutions</h2>
			</div>
			<!-- Start 2 -->
			<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   We offer both hosted and 3rd party hosted solutions for your business use case.  All you need to do is let us know what you want to create and our solutions team will help  </h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-online-store/02_hosted_3rd_party.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 2 -->
	 <div class="row title text-center margin-around">
				<h2>Customer Reviews</h2>
			</div>
			<!-- Start 3 -->
			<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-online-store/03_customer_reviews.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Showcasing your product or service reviews is a key step in any lead conversion funnel. It helps increase engagement by driving up trust for your offers.  Our Online Store solutions enable for customer reviews so they can leave their thoughts and experiences with you. As an administator of your Online Store reviews, you can moderate what is displayed and what gets used for employee/supplier training.</h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 3 -->	
	<!-- Start 4 -->		
			<div class="row title text-center margin-around">
				<h2>Incentivize and Reward</h2>
			</div>
			<div class="row white no-margin">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								    Your Merchantocracy Online Store comes with a robust discount code system so that you can keep customers focussed on completing the purchase.  You can customize your incentives or rewards to reflect specific spend thresholds, percentage discounts or you can design minimum order requests that activate incentives based on spend.  You can create what your imagination wants to create.</h5>
				</div>
            </div>
		</div>
        <div class="col-md-4 text-center box-main"><img src="/img/feat-online-store/05_incentives_rewards.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 4 -->	
     <!-- Start 6 -->
	<div class="row title text-center margin-around">
		<h2>Secure</h2>
	</div>
	<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-online-store/06_secure_payments.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   The Online Store platform requires encrypted and secure landing pages with SSL certificates and additional code to ensure your Merchantocracy Store is protecting your cardholders information at all stages of the buying cycle.</h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 5 -->
   	 <!-- Start 6 -->
	<div class="row title text-center margin-around">
		<h2>SEO Friendly</h2>
	</div>
	<div class="row white no-margin">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								    Today, you need to gain as much attention to your offers as possible, your Merchantocracy Store landing pages are SEO configurable so that you can add Title Tags, and Page descriptions that will help you market your offerings on search queries to more people, ongoingly, while you run your day to day business.</h5>
				</div>
            </div>
		</div>
        <div class="col-md-4 text-center box-main"><img src="/img/feat-online-store/07_seo_friendly.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 7 -->
	 </div>
	</section>
	<section class="section section-bot-padded section-transparent">
	</section>
	<section class="section section-slick-padded call-to-action">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="drk regular">Limited Time Offer</h3>
							<h4 class="drk regular">Sign up today and receive a 20% discount on your Online Store set up costs when you enrol before July 2018</h4>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="text-center">
							<h3 class="btn btn-xmain-xl"><a href="mailto:sales@merchantocracy.com" class="white" data-toggle="modal" data-target="#modal1">Ask us about our Free Merchant Program</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>