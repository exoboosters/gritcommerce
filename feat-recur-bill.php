	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="light white">Put Your Subscriptions On Autopilot</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl"><a href="mailto:sales@merchantocracy.com" class="white">Email Us To Set Up Trial Offer</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials" class="section">
		<div class="container">
			<!-- Start 1 -->
			<div class="row title text-center margin-around">
				<h2>Subscription Management</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-recur-bill/001_subscription_mgmt.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   With Merchantocracy, you can set up as many subscription plans as you like and run them all from one dashboard.  All you need to do is input the names, cardholders, payment frequency and time duration and the system will put those transactions on autopilot so that you can focus on your business.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 1 -->	
			<div class="row title text-center margin-around">
				<h2>Billing & Invoicing</h2>
			</div>
			<!-- Start 2 -->
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							Customized invoicing is as much about billing accurately, as it is about reflecting your brand. When you set up an invoicing account, you can choose to customize your billing strategy exactly the way you want to.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-recur-bill/002_billing_invoicing.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 2 -->
			<!-- Start 3 -->
			<div class="row title text-center margin-around">
				<h2>Payments & Accounting</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-recur-bill/003_payment_accounting.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   Today's customer wants to pay in a multitude of ways, with Merchantocracy you can impress your customer by offering credit card payment options, whatever makes it easier for them and supports their preferences.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 3 -->	
			<!-- Start 4 -->
			<div class="row title text-center margin-around">
				<h2>Reporting & Analytics</h2>
			</div>
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							When you offer a multitude of payment options, its nice to know that all of your data is in one place to manage.  Whether it be changing subscriptions or adding customers from around the world your back office reporting dashboard allows you to view the essentails to maintain a healthy business.  
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-recur-bill/004_reporting_analytics.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 4 -->
			<!-- Start 5 -->
			<div class="row title text-center margin-around">
				<h2>Payment Gateways</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-recur-bill/005_payment_gateways.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   You may require a specific payment gateway for your business do to costs or compatibility with 3rd party software. Merchantocracy offers you a development team that will make sure that your gateway connects with your website, workflow or business process. 
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 5 -->	
			<!-- Start 6 -->
			<div class="row title text-center margin-around">
				<h2>Email Integration Settings</h2>
			</div>
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							When it comes to communicating invoices or outstanding bills, our recurring billing system handles it all. All you need to do is preset the workflow triggers in the event that a credit card doesn't process or a balance isn't available.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-recur-bill/006_email_settings.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 6 -->
			<!-- Start 7 -->
			<div class="row title text-center margin-around">
				<h2>Customize Branding</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-recur-bill/007_custom_branding.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   Make sure that your brand sticks out in the minds of your customer, you can add messaging, icons and logo's to your invoices so that your customer knows and trusts the communication
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 7 -->	
	 </div>
	</section>
	<section class="section section-bot-padded section-transparent">
	</section>
	<section class="section section-slick-padded call-to-action">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="drk regular">Limited Time Offer</h3>
							<h4 class="drk regular">Sign up today and receive a 33% discount on your Recurring Billing solution when you enrol before July 2018</h4>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="text-center">
							<h3 class="btn btn-xmain-xl"><a href="mailto:sales@merchantocracy.com" class="white" data-toggle="modal" data-target="#modal1">Ask us about our Free Merchant Program</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>