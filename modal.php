    <!-- Modal Forms -->
	<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content modal-popup container-fluid">
				<a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
				<h3 class="white">Let us follow up with you</h3>
				<h4 class="white light">Schedule a 10 minute Demo and receive our 90 Day Introductory Trial offer</h4>
				<h5 class="light-white light">If you're open, perhaps we can schedule a quick 10-minute webinar/call.</h5>

				<h5 class="light-white light">If you fill out the information below, you will discover the perfect payment processing solution that will fulfill your customer's payment preferences exactly. Furthermore, we will not sell your information to any 3rd party company. All fields are required to receive the right solutions expert.</h5>
				
				
				
				<form action="/follow-up" class="popup-form" id="follow-up" method="post">
					
						<div class="row">
							<div class="col-md-6">
								<input type="text" class="form-control form-white" placeholder="First Name" required name="fname">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control form-white" placeholder="Last Name" required name="lname">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<input type="text" class="form-control form-white" placeholder="Company Name" required name="cname">
							</div>
							<div class="col-md-6">
								<input type="text" class="form-control form-white" placeholder="Website" required name="website">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<input type="text" class="form-control form-white" placeholder="Email Address" required name="email">
							</div>
							<div class="col-md-6">
								<input type="hidden" id="sel-best-time" name="sel-best-time" value="9:00 AM - 12:00 PM">
								<div class="dropdownf">
									<button id="dLabel" class="form-control form-white dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Best time to call <span class="pull-right"><i class="info-icon arrow_down"></i></span>
									</button>
									
									<ul class="dropdown-menu animated fadeIn" role="menu" id="best-time" aria-labelledby="dLabel">
										<li class="animated lightSpeedIn" value="9:00 AM - 12:00 PM"><a href="#">9:00 AM - 12:00 PM</a></li>
										<li class="animated lightSpeedIn" value="12:00 PM - 3:00 PM"><a href="#">12:00 PM - 3:00 PM</a></li>
										<li class="animated lightSpeedIn" value="3:00 PM - 6:00 PM"><a href="#">3:00 PM - 6:00 PM</a></li>
										<li class="animated lightSpeedIn" value="6:00 PM - 9:00 PM"><a href="#">6:00 PM - 9:00 PM</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<input type="text" class="form-control form-white" placeholder="Phone Number" required name="phone">
							</div>	
							<div class="col-md-6">
								<input type="text" class="form-control form-white" placeholder="Industry" required name="industry">
							</div>
						</div>
							
							
							<div class="row">
								<div class="col-md-4 col-md-offset-4">
									<button type="submit" class="btn btn-submit">Follow Up</button>
								</div>
							</div>
					
				</form>
			</div>
		</div>
	</div>
	<!-- Form 2 -->
	<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-signin">
			<div class="modal-content modal-popup">
				<a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
				<h3 class="white">Login</h3>
				<form action="" class="popup-form">
					<input type="text" class="form-control form-white" placeholder="User Name">
					<input type="password" class="form-control form-white" placeholder="Password">
					<div class="checkbox-holder text-left">
						<div class="checkbox">
							<input type="checkbox" value="None" id="squaredOne" name="check" />
							<label for="squaredOne"><span>Remember Me &nbsp;<i class="icon fa fa-question"></i></span></label>
						</div>
					</div>
					<div class="row">
								<div class="col-md-4 col-md-offset-4">
									<button type="submit" class="btn btn-submit">Submit</button>
								</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
