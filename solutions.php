	<section id="services" class="section section-padded drk-blue-bg">
		<div class="container">
			<div class="row text-center title">
				<h2 class="white">How can we help?</h2>
				<h4 class="light muted">See the different options we can provide</h4>
			</div>
			<div class="row services">
				<div class="col-md-3">
					<div class="service">
						<a href="/solutions/online-store">
							<div class="icon-holder">
								<img src="img/icons/001-online-store.png" alt="" class="icon">
							</div>
							<h4 class="heading">Online Store</h4>
							<p class="description">Start Selling your Offers using eCommerce</p>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<a href="/solutions/retail-pos">
							<div class="icon-holder">
								<img src="img/icons/002-retail-pos.png" alt="" class="icon">
							</div>
							<h4 class="heading">Retail Pos</h4>
							<p class="description">Upgrade your InStore Payment Experience</p>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<a href="/solutions/pos">
							<div class="icon-holder">
								<img src="img/icons/003-pos-terminals.png" alt="" class="icon">
							</div>
							<h4 class="heading">Point of Sale</h4>
							<p class="description">Choose any type of Payment Terminal</p>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<a href="/solutions/invoicing">
							<div class="icon-holder">
								<img src="img/icons/004-invoices.png" alt="" class="icon">
							</div>
							<h4 class="heading">Invoicing</h4>
							<p class="description">Email your invoices & Payment Options</p>
						</a>
					</div>
				</div>
			</div>
			<div class="row services">
				<div class="col-md-3">
					<div class="service">
						<a href="/solutions/virtual-terminals">
							<div class="icon-holder">
								<img src="img/icons/005-virtual-terminals.png" alt="" class="icon">
							</div>
							<h4 class="heading">Virtual Terminals</h4>
							<p class="description">Bill your Clients whenever convenient</p>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<a href="/solutions/web-integrations">
							<div class="icon-holder">
								<img src="img/icons/006-web-integrations.png" alt="" class="icon">
							</div>
							<h4 class="heading">Website Integrations</h4>
							<p class="description">Add Payment Gateways to your website</p>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<a href="/solutions/recurring-billing">
							<div class="icon-holder">
								<img src="img/icons/007-recurring-billing.png" alt="" class="icon">
							</div>
							<h4 class="heading">Recurring Billing</h4>
							<p class="description">Put retainers or Monthly Subscriptions on Autopilot.</p>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<a href="/solutions/lower-rates">
							<div class="icon-holder">
								<img src="img/icons/008-lower-rates.png" alt="" class="icon">
							</div>
							<h4 class="heading">Rates that are Lower</h4>
							<p class="description">We will provide Cost Plus Pricing.</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>