	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="light white">Maximize Your InStore Retail Experience</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl"><a href="mailto:sales@merchantocracy.com" class="white">Email Us To Set Up Trial Offer</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials" class="section">
		<div class="container">
			<div class="row title text-center margin-around">
				<h2>Cash Register Integration</h2>
			</div>
			<!-- Start 1 -->
			<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-retail-pos/POS_retail.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Wanna take your inStore brand experience up to a whole new level, check out our fully customizatble cash register system.  With a touch screen monitor interface, point of sale keyboard, you can create attractive, user-friendly screens to complete your point of sale system.  Each system comes with pre-designed templates that allow you to start processing with ease.</h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 1 -->	
			<div class="row title text-center margin-around">
				<h2>Inventory Management</h2>
			</div>
			<!-- Start 2 -->
			<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Knowing how long your inventory has been sitting on the shelf is extremely important, especially when you have net 30/60/90+ day suppliers. It is the key to maintaing a thriving bottomline as a result of efficient cashflow management. Know what inventory level you should be maintaining while also knowing what items are giving you the greatest financial returns.  </h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-retail-pos/inventory_management.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 2 -->
	 <div class="row title text-center margin-around">
				<h2>Sales Tracking & Reporting</h2>
			</div>
			<!-- Start 3 -->
			<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-retail-pos/sales_tracking.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   You can easily track what's currently selling, what time of the day/week/month/quarter your selling, and now you can include the difference between the actual hard net/net of purchasing/manufacturing cost and what you're selling at, so that you know your profit margin and can utilize your resources effectively.  If you need to track commissions for drivers, employees or sales staff, you can do that too. </h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 3 -->	
			<div class="row title text-center margin-around">
				<h2>Integrate 3rd Party Devices</h2>
			</div>
			<!-- Start 4 -->
			<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   Your Merchantocracy Retail System is compatible with 3rd party integrations because, you may need to run CRM's, Inventory Files, Reservations Software, Gift Card Tracking, Support Different Currencies while managing Float Maintenance. You can connect your Retail POS System to a multitude of 3rd party options.</h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-retail-pos/3rd_party_integrations.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 4 -->
	 <div class="row title text-center margin-around">
				<h2>Customize Invoices</h2>
			</div>
			<!-- Start 7 -->
			<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-retail-pos/custom_invoices.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								    You want your brand to look professional, thus you probably need a more robust invoicing solution to ensure your brand and return policies are communicated effectively.  Our integrated invoicing feature supports both backoffice suppliers/partners and in-store sales to that work seamlessly together.  You can process sales, generate invoices, receive payments on accounts by specific invoice and you can reprint aged account receivables (30/60/90+ days) if so desired.</h5>
				</div>
            </div>
		</div>
     </div>
     <!-- End 7 -->
	 <!--<div class="row title text-center margin-around">
				<h2>Reward Your Customers</h2>
			</div>-->
			<!-- Start 5 -->
			<!--<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-retail-pos/reward_customers.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								    You work so hard to earn the right to serve one customer.  Our Retail POS System will track the purchases your customers are making, allowing you to monitor the purchasing trends so that you can offer more rewards that keep them coming back for more.  We offer a reward point system based on spend or specific products purchased, either way, you want your customers to redeem their points on future purchases and we can help you bring them back.</h5>
				</div>
            </div>
		</div>
     </div>-->
     <!-- End 5 -->	
			<div class="row title text-center margin-around">
				<h2>Transaction Manager</h2>
			</div>
			<!-- Start 6 -->
			<div class="row white no-margin">
        
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								   In today's business environment, you need to make decisions about your business in real time, especially when managing complex inventory models, our transaction viewer provides real time insight into current or previous day activity, including reopening transactions from current or previous day transaction ledgers.  This way you can reprint any sales receipt from that day to help delvier better customer service  and support your own internal accounting team.</h5>
				</div>
            </div>
		</div>
		<div class="col-md-4 text-center box-main"><img src="/img/feat-retail-pos/txn_manager.png" alt="" class="img-responsive center-block"></div>
     </div>
     <!-- End 6 -->
	
	<!--<div class="row title text-center margin-around">
				<h2>Customize Invoices</h2>
			</div>-->
			<!-- Start 7 -->
			<!--<div class="row white no-margin">
        <div class="col-md-4 text-center box-main"><img src="/img/feat-retail-pos/custom_invoices.png" alt="" class="img-responsive center-block"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
								    You want your brand to look professional, thus you probably need a more robust invoicing solution to ensure your brand and return policies are communicated effectively.  Our integrated invoicing feature supports both backoffice suppliers/partners and in-store sales to that work seamlessly together.  You can process sales, generate invoices, receive payments on accounts by specific invoice and you can reprint aged account receivables (30/60/90+ days) if so desired.</h5>
				</div>
            </div>
		</div>
     </div>-->
     <!-- End 7 -->
	 </div>
	</section>
	<section class="section section-bot-padded section-transparent">
	</section>
	<section class="section section-slick-padded call-to-action">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="drk regular">Limited Time Offer</h3>
							<h4 class="drk regular">Sign up today and receive a 10% discount on your Retail POS solution set up costs when you enrol before July 2018</h4>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="text-center">
							<h3 class="btn btn-xmain-xl"><a href="mailto:sales@merchantocracy.com" class="white" data-toggle="modal" data-target="#modal1">Ask us about our Free Merchant Program</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>