	<header id="intro">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2 class="light white">The Secret To Happiness Is Collecting Payments From Anywhere</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="section section-bot-padded white-bg">
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center intro-tables">
							<h3 class="btn btn-main-xl"><a href="mailto:sales@merchantocracy.com" class="white">Email Us To Set Up Trial Offer</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>
	<section id="testimonials" class="section">
		<div class="container">
			<!-- Start 1 -->
			<div class="row title text-center margin-around">
				<h2>Accept Payments Remotely</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-virt-terminals/001-accept-remote-payments.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   If you're a small business and your looking to process card not present transactions from anywhere in the world that has internet access, then this is the ultimate solution for you.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 1 -->	
			<div class="row title text-center margin-around">
				<h2>Get Paid Quickly</h2>
			</div>
			<!-- Start 2 -->
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							Compared to other virtual terminals like PayPal for example, your payment will hit your bank account within 1-2 business days, with no holds.  Depending on the type of  business your operating and the amount of the transaction, this could make a real difference for your business.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-virt-terminals/002-paid-quickly.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 2 -->
			<!-- Start 3 -->
			<div class="row title text-center margin-around">
				<h2>Avoid Equipment Expenses</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-virt-terminals/003-avoid-expensive-equipment.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   If you don't need a physical terminal and your operating inside of a low risk industry, then the virtual terminal is an ideal solution because it cuts your "admin/monthly" costs down.  Virtual Terminal could be the ideal solution for your business and the access to freedom you've been searching for.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 3 -->	
			<div class="row title text-center margin-around">
				<h2>Helps Reduce PCI Compliance Workload</h2>
			</div>
			<!-- Start 4 -->
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							PCI compliance can be a significant cost to any business, we'll help you avoid this cost by becoming PCI Compliant. You don't have to endure any additional costs that could otherwise prevent your company from gaining access.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-virt-terminals/004-pci-compliance.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 4 -->
			<!-- Start 5 -->
			<div class="row title text-center margin-around">
				<h2>No Long Term Contracts</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-virt-terminals/005-no-long-term-contacts.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   This is the ultimate month to month solution from our administrative side, we're happy to honour this program for 90 days and if at that time you decide you need to upgrade or stop using our service all together, we'll help you accomplish that end.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 5 -->	
			<div class="row title text-center margin-around">
				<h2>See Every Payment In One Place</h2>
			</div>
			<!-- Start 6 -->
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							You can add user accounts with individual/unique permissions.  You can support purchases, pre-authorizations and adjustments using a customized  backoffice dashboard layout.  We offer drop down pick list menus, checkboxes and text fields and more.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-virt-terminals/006-reporting-analytics.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 6 -->
			<!-- Start 7 -->
			<div class="row title text-center margin-around">
				<h2>Put Payment Requests on Autopilot</h2>
			</div>
			<div class="row white no-margin">
				<div class="col-md-4 text-center box-main"><img src="/img/feat-virt-terminals/000-virtual-terminals.png" alt="" class="img-responsive center-block"></div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
						   After your account is approved, all you need to do is enter cardholder transaction details to process authorizations or refunds across Visa, Mastercard and Amex payment networks, it's super easy.
						   </h5>
						</div>
					</div>
				</div>
			</div>
			<!-- End 7 -->	
			<div class="row title text-center margin-around">
				<h2>Send Custom Email Recipts</h2>
			</div>
			<!-- Start 8 -->
			<div class="row white no-margin">        
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-12" style="padding: 5px; line-height: 2em; "><h5 class="heading">
							After the payment has been processed, you can send a fully branded email receipts straight to your clients inbox so that they have a record and feel good about the transaction.
							</h5>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center box-main"><img src="/img/feat-virt-terminals/007-custom-email-receipts.png" alt="" class="img-responsive center-block"></div>
			</div>
			<!-- End 8 -->
	 </div>
	</section>
	<section class="section section-bot-padded section-transparent">
	</section>
	<section class="section section-slick-padded call-to-action">
			<div class="cut cut-top-lb"></div>
			<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="item text-center">
							<h2 class="drk regular">Limited Time Offer</h3>
							<h4 class="drk regular">Sign up today and receive a 33% discount on your Virtual Terminal solution set up costs when you enrol before July 2018</h4>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						<div class="text-center">
							<h3 class="btn btn-xmain-xl"><a href="mailto:sales@merchantocracy.com" class="white" data-toggle="modal" data-target="#modal1">Ask us about our Free Merchant Program</a></h3>
						</div>
				</div>
			</div>
		</div>
	</section>