<?php

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

$f3 = require('f3/lib/base.php');

// Global Variables
$meta = '';
$title = '';
$inc_page = '';
$css_elements = '';
$prim_col = '';
$acce_col = '';
$acce_rgb = '';
$darker = '';
$darker_rgb = '';
$sub_level = '';
$cont_st_col = '';
$cont_en_col = '';
$home_logo = '/img/logo.png';

$f3->route('GET /',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col, $home_logo;
        $meta = "Merchantocracy is a brand new way for businesses to handle payments and invoices";
        $title = "Welcome to Merchantocracy";
        $inc_page = 'home.php';
        $css_elements = 'home';
		$prim_col = '#6a2c91';
		$acce_col = '#431162';
		$acce_rgb = '67, 17, 98';
		$darker = '#20082f';
		$darker_rgb = '32, 8, 47';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
		$home_logo = '/img/logo-active.png';
    }
);

$f3->route('POST /follow-up',
    function($f3) {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col, $home_logo;
        $meta = "Merchantocracy is a brand new way for businesses to handle payments and invoices";
        $title = "We will Follow up";
        $inc_page = 'followup.php';
        $css_elements = 'followup';
		$prim_col = '#6a2c91';
		$acce_col = '#431162';
		$acce_rgb = '67, 17, 98';
		$darker = '#20082f';
		$darker_rgb = '32, 8, 47';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
		$home_logo = '/img/logo-active.png';
		
    }
);

$f3->route('GET /oldhome',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col, $home_logo;
        $meta = "Merchantocracy is a brand new way for businesses to handle payments and invoices";
        $title = "Welcome to Merchantocracy";
        $inc_page = 'home.php';
        $css_elements = 'home';
		$prim_col = '#77e1c5';
		$acce_col = '#143028';
		$acce_rgb = '20, 48, 40';
		$darker = '#081a15';
		$darker_rgb = '8, 26, 21';
		$cont_st_col = '#45484d';
		$cont_en_col = '#000000';
		$home_logo = '/img/logo-active.png';
    }
);

$f3->route('GET /about',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb;
        $meta = "Learn more about Merchantocracy";
        $title = "About Merchantocracy";
        $inc_page = 'about.php';
        $css_elements = 'about';
    }
);

$f3->route('GET /solutions/retail-pos',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col;
        $meta = "Upgrade your Instore Payment Experience";
        $title = "Merchantocracy: Retail Pos";
        $inc_page = 'feat-retail-pos.php';
        $css_elements = 'feat-retail-pos';
		$prim_col = '#9761c8';
		$acce_col = '#311649';
		$acce_rgb = '49, 22, 73';
		$darker = '#190828';
		$darker_rgb = '25, 8, 40';
		$sub_level = '../';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
    }
);

$f3->route('GET /solutions/online-store',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col;
        $meta = "Things You Ought To Know About Processing Online Store Payments";
        $title = "Merchantocracy: Online Store";
        $inc_page = 'feat-online-store.php';
        $css_elements = 'feat-online-store';
		$prim_col = '#f0525a';
		$acce_col = '#b22f35';
		$acce_rgb = '178, 47, 53';
		$darker = '#631316';
		$darker_rgb = '99, 19, 22';
		$sub_level = '../';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
    }
);

$f3->route('GET /solutions/pos',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col;
        $meta = "Upgrade Your Payment Experience With Lightning Fast Terminals";
        $title = "Merchantocracy: POS";
        $inc_page = 'feat-pos.php';
        $css_elements = 'feat-pos';
		$prim_col = '#fcc848';
		$acce_col = '#dca51d';
		$acce_rgb = '220, 165, 29';
		$darker = '#7b5905';
		$darker_rgb = '123, 89, 5';
		$sub_level = '../';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
    }
);

$f3->route('GET /solutions/invoicing',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col;
        $meta = "Collect Payments With Ease";
        $title = "Merchantocracy: Invoicing";
        $inc_page = 'feat-invoicing.php';
        $css_elements = 'feat-invoicing';
		$prim_col = '#fa9a46';
		$acce_col = '#d97720';
		$acce_rgb = '217, 119, 32';
		$darker = '#9f5411';
		$darker_rgb = '159, 84, 17';
		$sub_level = '../';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
    }
);

$f3->route('GET /solutions/virtual-terminals',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col;
        $meta = "The Secret To Happiness Is The Freedom To Collect Payments Anywhere";
        $title = "Merchantocracy: Virtual Terminals";
        $inc_page = 'feat-virt-terminals.php';
        $css_elements = 'feat-virt-terminals';
		$prim_col = '#537ed6';
		$acce_col = '#24458a';
		$acce_rgb = '36, 69, 138';
		$darker = '#0b1d43';
		$darker_rgb = '11, 29, 67';
		$sub_level = '../';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
    }
);

$f3->route('GET /solutions/web-integrations',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col;
        $meta = "Effortlessly Partner With Any 3rd Party Web Applications ";
        $title = "Merchantocracy: Website Integrations";
        $inc_page = 'feat-website-int.php';
        $css_elements = 'feat-website-int';
		$prim_col = '#de5fa6';
		$acce_col = '#992666';
		$acce_rgb = '153, 38, 102';
		$darker = '#3f0827';
		$darker_rgb = '63, 8, 39';
		$sub_level = '../';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
    }
);

$f3->route('GET /solutions/recurring-billing',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col;
        $meta = "Put Your Subscriptions On Autopilot";
        $title = "Merchantocracy: Recurring Billing";
        $inc_page = 'feat-recur-bill.php';
        $css_elements = 'feat-recur-bill';
		$prim_col = '#b64926';
		$acce_col = '#833218';
		$acce_rgb = '131, 50, 24';
		$darker = '#341207';
		$darker_rgb = '52, 18, 7';
		$sub_level = '../';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
    }
);

$f3->route('GET /solutions/lower-rates',
    function() {
        global $meta, $title, $inc_page, $css_elements, $prim_col, $acce_col, $darker, $acce_rgb, $darker_rgb, $sub_level, $cont_st_col, $cont_en_col;
        $meta = "The Secret Is Joining The Right Rate Program";
        $title = "Merchantocracy: Lower Rates";
        $inc_page = 'feat-rates.php';
        $css_elements = 'feat-rates';
		$prim_col = '#56c22d';
		$acce_col = '#3d8e1e';
		$acce_rgb = '61, 142, 30';
		$darker = '#153708';
		$darker_rgb = '21, 55, 8';
		$sub_level = '../';
		$cont_st_col = '#ffffff';
		$cont_en_col = '#cccccc';
    }
);

$f3->run();

// Header, up until hero image
include 'header.php';

include($inc_page);

// Modal Forms
include 'modal.php';

// Footer, just the footer section
include 'footer.php';